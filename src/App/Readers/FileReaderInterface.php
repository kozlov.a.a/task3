<?php
/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 21.10.2017
 * Time: 8:42
 */

namespace App\Readers;

/**
 * Interface FileReaderInterface
 * @package App
 */
interface FileReaderInterface
{
    /**
     * FileReaderInterface constructor.
     * @param string $fileName
     */
    public function __construct(string $fileName);

    /**
     * Read from current position
     * @return string
     */
    public function readCurrent(): string;

    /**
     * Return current position
     * @return int
     */
    public function getPosition(): int;

    /**
     * @param int $newPosition
     * @throws \OutOfBoundsException
     */
    public function changePosition(int $newPosition): void;
}
