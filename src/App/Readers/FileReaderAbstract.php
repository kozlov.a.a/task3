<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 21.10.2017
 * Time: 16:10
 */

namespace App\Readers;

use App\Readers\Exceptions\FileReaderExceptions;

/**
 * Class FileReaderAbstract
 * @package App
 */
abstract class FileReaderAbstract implements FileReaderInterface
{
    /**
     * @var resource
     */
    private $fileHandle;

    /**
     * FileReaderAbstract constructor.
     * @param string $fileName
     * @throws FileReaderExceptions
     */
    public function __construct(string $fileName)
    {
        $this->fileHandle = fopen($fileName, 'rb');

        if (false === $this->fileHandle) {
            throw new FileReaderExceptions('Cannot open the file');
        }
    }

    /**
     * @return \resource
     */
    protected function getFileHandle()
    {
        return $this->fileHandle;
    }
}
