<?php
/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 21.10.2017
 * Time: 16:31
 */

namespace App\Readers\Exceptions;

use App\Exceptions\AppException;

/**
 * Class FileReaderExceptions
 * @package App\Readers\Exceptions
 */
class FileReaderExceptions extends AppException
{
}
