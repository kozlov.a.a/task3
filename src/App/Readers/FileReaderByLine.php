<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 21.10.2017
 * Time: 8:42
 */

namespace App\Readers;

/**
 * Class FileByLineReader
 * @package App
 */
class FileReaderByLine extends FileReaderAbstract
{
    /**
     * current line number
     * @var int
     */
    private $position = 0;

    /**
     * count lines in file
     * @var int
     */
    private $countLines;

    /**
     * current value
     * @var string
     */
    private $currentValue;

    /**
     * @inheritdoc
     */
    public function __construct(string $fileName)
    {
        parent::__construct($fileName);
        $this->calcCountLines();
        $this->nextLine();
    }

    /**
     * @inheritdoc
     */
    public function readCurrent(): string
    {
        return $this->currentValue;
    }

    /**
     * @inheritdoc
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @inheritdoc
     */
    public function changePosition(int $newPosition): void
    {
        $this->changeCurrentLine($newPosition);
    }

    /**
     * calc count lines in file
     */
    private function calcCountLines()
    {
        $this->countLines = 1;

        $this->rewind();

        while (!feof($this->getFileHandle())) {
            ++$this->countLines;
            $this->nextLine();
        }

        $this->rewind();
    }

    /**
     * @param int $position
     * @throws \OutOfBoundsException
     */
    private function changeCurrentLine(int $position)
    {
        if ($position > $this->countLines) {
            throw new \OutOfBoundsException('');
        }

        if ($this->position > $position) {
            $this->rewind();
        }

        $offset = $position - $this->position;

        while (--$offset >= 0) {
            $this->nextLine();
        }
    }

    /**
     * reset file pointer
     */
    public function rewind()
    {
        rewind($this->getFileHandle());
        $this->position = 0;
        $this->nextLine();
    }

    /**
     * change pointer to next line
     */
    private function nextLine()
    {
        ++$this->position;
        $this->currentValue = fgets($this->getFileHandle());
    }
}
