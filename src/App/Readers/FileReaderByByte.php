<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 21.10.2017
 * Time: 8:43
 */

namespace App\Readers;

/**
 * Class FileByByteReader
 * @package App
 */
class FileReaderByByte extends FileReaderAbstract
{
    /**
     * @var int
     */
    private $fileSize;

    /**
     * count bytes for read
     */
    const READ_LENGTH = 1;

    /**
     * @inheritdoc
     */
    public function __construct(string $fileName)
    {
        parent::__construct($fileName);
        $this->fileSize = fstat($this->getFileHandle())['size'];
    }

    /**
     * @return string
     */
    public function readCurrent(): string
    {
        $data = fread($this->getFileHandle(), self::READ_LENGTH);

        fseek($this->getFileHandle(), -self::READ_LENGTH, SEEK_CUR);

        return $data;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return ftell($this->getFileHandle());
    }

    /**
     * @param int $newPosition
     * @throws \OutOfBoundsException
     */
    public function changePosition(int $newPosition): void
    {
        if ($this->checkEOF() || fseek($this->getFileHandle(), $newPosition)!==0) {
            throw new \OutOfBoundsException('');
        }
    }

    /**
     * @return bool
     */
    private function checkEOF(): bool
    {
        return  $this->getPosition() >= $this->fileSize;
    }
}
