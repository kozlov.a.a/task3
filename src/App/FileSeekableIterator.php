<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 19.10.2017
 * Time: 7:53
 */

namespace App;

use App\Readers\FileReaderInterface;

/**
 * Class FileSeekableIterator
 * @package App
 */
class FileSeekableIterator implements \SeekableIterator
{
    /**
     * @var bool
     */
    private $valid;

    /**
     * @var FileReaderInterface
     */
    private $fileReader;

    /**
     * FileSeekableIterator constructor.
     * @param FileReaderInterface $fileReader
     */
    public function __construct(FileReaderInterface $fileReader)
    {
        $this->fileReader = $fileReader;
    }

    /**
     * @inheritdoc
     */
    public function current(): string
    {
        return $this->fileReader->readCurrent();
    }

    /**
     * @inheritdoc
     */
    public function next(): void
    {
        try {
            $this->seek($this->key() + 1);
            $this->valid = true;
        } catch (\OutOfBoundsException $exception) {
            $this->valid = false;
        }
    }

    /**
     * @inheritdoc
     */
    public function key()
    {
        return $this->fileReader->getPosition();
    }

    /**
     * @inheritdoc
     */
    public function valid(): bool
    {
        return $this->valid;
    }

    /**
     * @inheritdoc
     */
    public function rewind(): void
    {
        try {
            $this->seek(0);
            $this->valid = true;
        } catch (\OutOfBoundsException $exception) {
            $this->valid = false;
        }
    }

    /**
     * @inheritdoc
     * @throws \OutOfBoundsException
     */
    public function seek($position): void
    {
        $this->fileReader->changePosition($position);
    }
}
