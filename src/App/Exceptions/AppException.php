<?php
/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 21.10.2017
 * Time: 16:29
 */

namespace App\Exceptions;

use Exception;

/**
 * Interface ExceptionInterface
 * @package App\Exceptions
 */
class AppException extends Exception
{
}
